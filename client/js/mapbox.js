var mapboxClient;
var map;
var features = [];

function createMap() {
    map = new mapboxgl.Map({
        container: "map",
        style: "mapbox://styles/mapbox/streets-v11",
        zoom: 15
    });
    mapboxClient = mapboxSdk({
        accessToken: mapboxgl.accessToken
    });

    map.addControl(
        new mapboxgl.NavigationControl(),
        "bottom-right"
    );
}

function addLocation(address, markerTitle, content) {
    mapboxClient.geocoding
        .forwardGeocode({
            query: address,
            autocomplete: false,
            limit: 1
        })
        .send()
        .then(function (response) {
            if (
                response &&
                response.body &&
                response.body.features &&
                response.body.features.length
            ) {
                var feature = response.body.features[0];
                var marker = new mapboxgl.Marker()
                    .setLngLat(feature.center)
                    .setPopup(createPopup(markerTitle, content))
                    .addTo(map);
                features.push(feature);
                centerMap();
            }
        });
}

function createPopup(markerTitle, content) {
    return new mapboxgl.Popup().setHTML(
        `<div id="content">
                        <h3><small>${markerTitle}</small></h3>
                        <div id="bodyContent"><p>${content}</p></div>
            </div>`
    );
}

function centerMap() {
    if (features.length == 1) {
        map.setCenter(features[0].center);
    } else if (features.length > 1) {
        var coordinates = [];
        features.forEach(feature => {
            coordinates.push(feature.geometry.coordinates);
        });
        var bounds = coordinates.reduce(function (bounds, coord) {
            return bounds.extend(coord);
        }, new mapboxgl.LngLatBounds(
            coordinates[0],
            coordinates[0]
        ));

        map.fitBounds(bounds, {
            maxZoom: 15,
            padding: 100
        });
    }
}