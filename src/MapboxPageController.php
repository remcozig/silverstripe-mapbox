<?php

namespace Remcozig\Mapbox;

use PageController;
use SilverStripe\View\Requirements;

class MapboxPageController extends PageController
{
    protected function init()
    {
        parent::init();
        Requirements::javascript('https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.js');
        Requirements::css('https://api.tiles.mapbox.com/mapbox-gl-js/v1.5.0/mapbox-gl.css');
        Requirements::javascript('https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js');
        Requirements::javascript('remcozig/silverstripe-mapbox:client/js/mapbox.js');
    }
}
