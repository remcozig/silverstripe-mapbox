# Silverstripe-Mapbox

Mapbox pagina's voor Silverstripe

## Handleiding
[x] Kopieer mapbox.template naar je eigen project en hernoem het naar mapbox.yml
[x] Plaats een Mapbox API key in de config file. Een API key kan aangemaakt worden op mapbox.com
