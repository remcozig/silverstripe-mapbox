<?php

namespace Remcozig\Mapbox;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Versioned\Versioned;
use Remcozig\Mapbox\MapboxPage;

class MapboxMarker extends DataObject
{
	private static $table_name = 'mapbox_marker';

	private static $versioned_gridfield_extensions = true;

	private static $db = array(
		'Title' => 'Varchar(255)',
		'Location' => 'Varchar(255)',
		'Content' => 'Text'
	);

	private static $has_one = array(
		'MapboxPage' => MapboxPage::class,
	);

	private static $summary_fields = array(
		'Title',
		'Location',
	);

	private static $extensions = [
		Versioned::class,
	];

	public function fieldLabels($includerelations = true)
	{
		$labels = parent::fieldLabels(true);

		$labels['Title'] = _t(__CLASS__ . '.Title', 'Titel');
		$labels['Location'] = _t(__CLASS__ . '.Location', 'Location');

		return $labels;
	}


	public function getCMSFields()
	{
		return FieldList::create(
			TextField::create('Title', _t(__CLASS__ . '.Title', 'Title')),
			TextField::create('Location', _t(__CLASS__ . '.Location', 'Location')),
			TextareaField::create('Content', _t(__CLASS__ . '.Content', 'Content'))
		);
	}
}
