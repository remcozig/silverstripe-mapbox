<?php

namespace Remcozig\Mapbox;

use Page;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use Remcozig\Mapbox\MapboxMarker;

class MapboxPage extends Page
{
    private static $table_name = 'mapbox_page';

    private static $db = array();

    private static $has_many = array(
        'Markers' => MapboxMarker::class,
    );

    private static $owns = [
        'Markers'
    ];

    function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $config = GridFieldConfig_RecordEditor::create();
        $config->getComponentByType(GridFieldSortableHeader::class)->setFieldSorting(array('Title'));

        $fields->addFieldToTab('Root.Markers', GridField::create(
            'Markers',
            _t(__CLASS__ . '.Markers', "Markers on the map"),
            $this->Markers()->sort('Title'),
            $config
        ));

        $fields->findOrMakeTab("Root.Markers")->setTitle(_t(__CLASS__ . '.markersTab', "Markers"));

        return $fields;
    }

    public function MapboxApiKey()
    {
        return $this->config()->mapbox_api_key;
    }
}
